<html>
	<head>
		<title> @yield('titel') </title>
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                <link rel="stylesheet" href="/css/kidsbuzz.css">
                <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="/css/app.css">
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                   
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
                <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
                <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
                <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
                
                
               
        @yield('scripts')
	</head>
	<body>
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                      <!-- Brand and toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/home/index"><img src="/img/logo.png" id="logopic"></a>
                      </div>

                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                       <ul class="nav navbar-nav">
                          <li class="active"><a href="/search/form">Zoeken <span class="sr-only">(current)</span></a></li>
                         
                          
                              
                          
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categorieën <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="/search/category/1">Taal</a></li>
                              <li><a href="/search/category/2">Rekenen</a></li>
                              <li><a href="/search/category/3">Entertainment</a></li>
                              <li><a href="/search/category/4">Persoonlijke ontwikkeling</a>
                             <!-- <li class="divider"></li> -->
                              <li><a href="/search/category/5">Geheugen</a></li>
                              <li><a href="/search/category/6">Behendigheid</a></li>
                              <li><a href="/search/category/7">Logisch denkvermogen</a>
                            </ul>
                          </li>
                           @if (Session::has('leeftijd'))
                               <li class="dropdown">
                                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gekozen leeftijd: {{Session::get('leeftijd')}} <span class="caret"></span></a>
                                   <ul class="dropdown-menu" role="menu">
                                       <li><a href="/unset">Gekozen leeftijd verwijderen</a></li>
                                   </ul>
                               </li>
                               @endif
                        </ul> 

                        <form action="/search/query" method="POST" class="navbar-form navbar-left" role="search">
                          <div class="form-group">
                              <input name="_token" hidden value="{!! csrf_token() !!}" />
                            <input name="zoekopdracht" type="text" class="form-control" placeholder="Zoeken">
                          </div>
                          <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        </form>
                          <ul class="nav navbar-nav navbar-right">
                              @if (Auth::guest())
                                  <li><a href="{{ url('/auth/login') }}">Login</a></li>
                                  <li><a href="{{ url('/auth/register') }}">Register</a></li>
                              @else
                                  <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                                      <ul class="dropdown-menu" role="menu">
                                          @if (Auth::user()->user_type == "2")
                                          <li><a href="/admin/create">Nieuwe applicatie toevoegen</a></li>
                                          @endif
                                          <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                                      </ul>
                                  </li>
                              @endif
                          </ul>
                      </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->

                 </nav>
            
     
                <header>            
                   
                <div id="wrapper">
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    {{-- @yield word gebruikt om een sectie te definieren. naam van de sectie staat tussen haakjes in de @yield) --}}  
                    @yield('content')
                </div>

                    @yield('footer')
	</body>
</html>
