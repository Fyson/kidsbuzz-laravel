{{-- Hier word vertelt dat de volgende in de master.blade.php word ingevuld.) --}}
@extends('master')

{{-- @section = welke sectie met wat word ingevuld op de master pagina.) --}}
@section('titel')
    Nieuwe applicatie toevoegen


    {{-- @stop komt na elke het einde van elke sectie.) --}}
@stop


@section('content')
<h1>Nieuwe app toevoegen</h1>

    {!! Form::open(['action' => 'AdminController@store', 'files' => true]) !!}
    <div class="form-group">
        {!! Form::label('name', 'Naam') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>

<div class="form-group">
    {!! Form::label('description', 'Omschrijving') !!}
    {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('minimal', 'Minimale leeftijd ') !!}
    {!! Form::selectRange('minimal', 0, 12, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('maximal', 'Maximale leeftijd ') !!}
    {!! Form::selectRange('maximal', 0, 12, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('category', 'Categorie ') !!}
    {!! Form::select('category[]', $categorie, null, ['id'=>'category_list', 'class'=>'form-control','multiple']) !!}
</div>

<div class="form-group">
    {!! Form::label('android_link', 'Google Play Store link') !!}
    {!! Form::text('android_link', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('ios_link', 'Apple App Store link') !!}
    {!! Form::text('ios_link', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('android_price', 'Google Play Store prijs') !!}
    {!! Form::text('android_price', null, array('class'=>'form-control', 'placeholder' => 'Format: &euro;X,XX of Gratis')) !!}
</div>

<div class="form-group">
    {!! Form::label('ios_price', 'Apple App Store prijs') !!}
    {!! Form::text('ios_price', null, array('class'=>'form-control', 'placeholder' => 'Format: &euro;X,XX of Gratis')) !!}
</div>

<div class="form-group">
    {!! Form::label('pic', 'Pictogram') !!}
    {!! Form::file('pic', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('image', 'Afbeeldingen/Screenshots') !!}
    {!! Form::file('image', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit('Toevoegen', ['class'=>'btn btn-primary form-control']) !!}
</div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@section('footer')
    <script>
        $('#category_list').select2({
                    placeholder: 'Kies een categorie'
                }
        );
    </script>
@endsection
@stop