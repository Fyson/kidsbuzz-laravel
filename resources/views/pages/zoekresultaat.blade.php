{{-- Hier word vertelt dat de volgende in de master.blade.php word ingevuld.) --}}
@extends('master')

{{-- welke sectie met wat word ingevuld op de master pagina.) --}}
@section('titel')
    Detail pagina

    {{-- @stop komt na het einde van elke sectie.) --}}
@stop


@section('content')

@if (!isset($rows))


    @elseif (empty($rows)||$rows=="")
        Er zijn geen apps gevonden
        @endif


    @foreach ($zoekresultaten as $app)
        <div class="pictogram col-xs-5 col-xs-offset-0 col-md-2 col-md-offset-0">

            <a href="/detail/{{ $app->id }}"> <img src="{{$app->pic}}">

                 {{ $app->name }}
            </a> </br>


            @foreach ($app->categories as $category)

                 {{ $category->name }} /
                    @endforeach

                @if ($app->minimal === $app->maximal)
                    {{ $app->minimal }} jr. 
                @endif

                @if($app->minimal !== $app->maximal)
                    {{ $app->minimal }} - {{ $app->maximal }} jr. 
                @endif
    </div>
            @endforeach
        
@stop
