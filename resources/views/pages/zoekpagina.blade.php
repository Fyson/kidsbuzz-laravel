{{-- Hier word vertelt dat de volgende in de master.blade.php word ingevuld.) --}}
@extends('master')

{{--  welke sectie met wat word ingevuld op de master pagina.) --}}
@section('titel')
        Zoek pagina

@stop               
{{--  @stop komt na elke het einde van elke sectie.) --}}   

@section('content')
{{--Zoekformulier--}}
{!!Form::open(array('url'=>'/search/query'))!!}
{!!Form::text('zoekopdracht', null, array('placeholder'=>'Zoeken'))!!}<br>
    {!!Form::radio('prijs', 'alle', true)!!}Alle<br>
    {!!Form::radio('prijs', 'gratis')!!}Gratis<br>
    {!!Form::radio('prijs', 'betaald')!!}Betaald<br>
    {!!Form::submit('Zoeken')!!}
    {!!Form::close()!!}<br><br><br>

{{--Leeftijd zoekformulier--}}
{!!Form::open(array('url'=>'/search/age'))!!}    
{!! Form::selectRange('leeftijd', 0, 12) !!}
 {!!Form::submit('Zoeken')!!}
{!!Form::close()!!}<br><br><br>

{{--Categorie zoekformulier--}}
{!!Form::open(array('url'=>'/search/category'))!!}    
{!! Form::select('categorie', $categorie) !!}
 {!!Form::submit('Zoeken')!!}
{!!Form::close()!!}<br><br><br>  

@stop