{{-- Hier word vertelt dat de volgende in de master.blade.php word ingevuld.) --}}
@extends('master')

@section('scripts')

    <script type="text/javascript" src="/js/expanding.js"></script>
@stop

{{-- welke sectie met wat word ingevuld op de master pagina.) --}}
@section('titel')
    Detail pagina

    {{-- @stop komt na elke het einde van elke sectie.) --}}
@stop


@section('content')
<div class="col-xs-4 col-xs-offset-3 col-md-2 col-md-offset-0">
    <img id="detailpictogram" src="{{$app->pic}}">
</div> 

<div class="col-xs-12 col-md-8">
    
    <section id="detailinfo">
       
    {{$app->name}}
    </br>
        <i>
            @foreach ($app->categories as $category)
              {{ $category->name }} /
            @endforeach 
            <!--Als de minimale en de maximale leeftijd gelijk zijn laat alleen de minimale leeftijd zien-->
             @if ($app->minimal === $app->maximal)
                {{ $app->minimal }} jr. 
             @endif
            <!--Als de minimale- en de maximale leeftijd niet gelijk zijn laat allebei zien.-->
             @if($app->minimal !== $app->maximal)
                {{ $app->minimal }} - {{ $app->maximal }} jr. 
             @endif </br>
        </i>
    
        <span>
            @if (!empty($app->android_price))
                <p>Google Play Store: {{$app->android_price}}</p>
            @endif

            @if (!empty($app->ios_price))
                <p>Apple App Store: {{$app->ios_price}}</p>
            @endif
        </span>
       
        
    
    </section>
</div>  

    <div class="col-xs-12 col-md-2 col-md-offset-0">
    <section class="starrating">
            <p>
                @for ($i=1; $i <= 5 ; $i++)
                <span class="glyphicon glyphicon-star{{ ($i <= $app->rating_cache) ? '' : '-empty'}}"></span>
                @endfor
                {{ number_format($app->rating_cache, 1)}} sterren
            </p>
        @if (Auth::user()->user_type == "2")
            <a href="/admin/{{ $app->id }}/edit">Applicatie bewerken</a>
        @endif
    </section>
    </div>



<div id="myCarousel" class="carousel slide col-xs-12 col-md-8 col-md-offset-2" data-ride="carousel">
  <!-- Indicators
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol> -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
     @foreach ($app->images as $index => $image)
          <div class="item @if($index == 0) {{ 'active' }} @endif">
              <img src="{{ $image->img_path }}">
          </div>
      @endforeach
  </div>

  @if ($imgcount > 1)
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
      @endif
</div>



    <div class="col-xs-12 col-md-8 col-md-offset-2">
     <article class="descriptie">
        {{$app->description}} 
     </article>
    </div>

<div class="col-md-6 col-xs-12 col-md-offset-2">
    <section class="link">  
        @if (!empty($app->android_link))
            <a href="{{$app->android_link}}">Klik hier om hem te bekijken in de Google Play Store.</a><br><br>
        @endif
        

             @if (!empty($app->ios_link))
             <a href="{{$app->ios_link}}">Klik hier om hem te bekijken in de Apple App Store.</a><br><br>
            @endif

            <p>Geüpload op: {{$app->created_at}}</p>
    </section>
</div>  
    <!-- <div class="ratings">
        <p class="pull-right">{{$app->rating_count}} {{ Str::plural('review', $app->rating_count)}}</p>
        <p>
            @for ($i=1; $i <= 5 ; $i++)
                <span class="glyphicon glyphicon-star{{ ($i <= $app->rating_cache) ? '' : '-empty'}}"></span>
            @endfor
            {{ number_format($app->rating_cache, 1)}} stars
        </p>
    </div> -->
    
          
   <div class="container">

        <div class="row" style="margin-top:40px;">
            <div class="col-md-6 col-xs-12 col-md-offset-2">
                <div class="well well-sm">
                    @if (Auth::check())
                    <div class="row">
                        <div class="col-md-12">
                            @if(Session::get('errors'))
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5>There were errors while submitting this review:</h5>
                                    @foreach($errors->all(':message') as $message)
                                        {{$message}}
                                    @endforeach
                                </div>
                            @endif
                            @if(Session::has('review_posted'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5>Your review has been posted!</h5>
                                </div>
                            @endif
                            @if(Session::has('review_removed'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h5>Your review has been removed!</h5>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="text-right">
                        <a href="#reviews-anchor" id="open-review-box" class="btn btn-success btn-green">Leave a Review</a>
                    </div>
                    <div class="row" id="post-review-box" style="display:none;">
                        <div class="col-md-12">
                            {!!Form::open()!!}
                            {!!Form::hidden('rating', null, array('id'=>'ratings-hidden'))!!}
                            {!!Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))!!}
                            <div class="text-right">
                                <div class="stars starrr" data-rating="{{Input::old('rating',0)}}"></div>
                                <a href="#" class="btn btn-danger btn-sm" id="close-review-box" style="display:none; margin-right:10px;"> <span class="glyphicon glyphicon-remove"></span>Cancel</a>
                                <button class="btn btn-success btn-lg" type="submit">Save</button>
                            </div>
                            {!!Form::close()!!}
                        </div>
                    </div>
                    @endif
                    @foreach($reviews as $review)
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                @for ($i=1; $i <= 5 ; $i++)
                                    <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                                @endfor

                                {{ $review->user ? $review->user->name : 'Anonymous'}} <span class="pull-right">{{$review->timeago}}</span>

                                <p>{{{$review->comment}}}</p>
                            </div>
                        </div>
                    @endforeach

                  <!--  <a href="/review/{{$app->id}}">Alle Recensies</a> -->
                </div>

            </div>
        </div>
    </div>

    <br><br>


@stop