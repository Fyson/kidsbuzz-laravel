{{-- Hier word vertelt dat de volgende in de master.blade.php word ingevuld.) --}}
@extends('master')

{{-- @section = welke sectie met wat word ingevuld op de master pagina.) --}}
@section('titel')
    Homepage


    {{-- @stop komt na elke het einde van elke sectie.) --}}
@stop


@section('content')
    <?php $leeftijd = Session::get('leeftijd');?>

    @if (empty($leeftijd))
<div id="blackground">

</div>

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Wat is uw kind's leeftijd?</h4>
      </div>
      <div class="modal-body">

          {{--Leeftijd zoekformulier--}}
            {!!Form::open(array('action'=>'PagesController@popup'))!!}
            {!! Form::selectRange('leeftijd', 0, 12) !!}
           {!!Form::submit('Kies')!!}
          {!!Form::close()!!}

    </div>


    <div class="modal-footer">
      <button type="button" class="btn btn-primary" name="leeftijd" value="alle"><a href="/popup1">Weergeef alle leeftijden</a></button>
    </div>

  </div>
</div>
@endif

 <div class="row">

      @foreach ($categorie as $cat)
     <div class="col-xs-12 col-md-6 col-md-offset-0">
         <h1 class="titel"><a href="/search/category/{{ $cat->id }}"> {{ $cat->name }} </a></h1>
@if ($cat->applications->count() !== 0)
           @foreach ($cat->applications->take(4) as $app)
               <div class="pictogram col-xs-5 col-xs-offset-0 col-md-3 col-md-offset-0">
                   <a href="/detail/{{ $app->id }}"> <img src="{{$app->pic}}">

                       {{ $app->name }} </a> </br>

                   <!--Laat de bijbehorende categorieën zien-->
                   @foreach ($app->categories as $category)

                       {{ $category->name }} /
                       @endforeach
                               <!--Als de minimale en de maximale leeftijd gelijk zijn laat alleen de minimale leeftijd zien-->
                       @if ($app->minimal === $app->maximal)
                           {{ $app->minimal }} jr.
                           @endif
                                   <!--Als de minimale- en de maximale leeftijd niet gelijk zijn laat allebei zien.-->
                           @if($app->minimal !== $app->maximal)
                               {{ $app->minimal }} - {{ $app->maximal }} jr.
                           @endif

               </div>
               @endforeach


@else <p>Er zijn geen apps gevonden in deze categorie</p>

    @endif


      </div>
        @endforeach


    </div>
  <?php

  /*$app = $app->whereId("4")->first();

  // Apple of android
  $Apple = array();
  $Apple['UA'] = $_SERVER['HTTP_USER_AGENT'];
  $Apple['Device'] = false;
  $Apple['Types'] = array('iPhone', 'iPod', 'iPad','Android');
  foreach ($Apple['Types'] as $d => $t) {
      $Apple[$t] = (strpos($Apple['UA'], $t) !== false);
      $Apple['Device'] |= $Apple[$t];
  }
    if($Apple['iPhone']){
        $OS = "Ios";
        echo"yay iphone";
    }
    if
    ($Apple['iPad']){
        $OS = "Ios";
        echo"yay ipad";
    }
     if
     ($Apple['Android']){
      $OS = "Android";
         echo"yay android";
    }


  ;


  // is this an Apple device?
  //echo
  //"<p>Apple device? ", ($Apple['Device'] ? 'true' : 'false'),
  //"</p>n<p>iPhone? ", ($Apple['iPhone'] ? 'true' : 'false'),
  //"</p>n<p>iPod? ", ($Apple['iPod'] ? 'true' : 'false'),
  //"</p>n<p>iPad? ", ($Apple['iPad'] ? 'true' : 'false'),
  //'</p>'; */
  ?>








@stop
