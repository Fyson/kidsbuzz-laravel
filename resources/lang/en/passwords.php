<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Uw wachtwoord moet minimaal zes karakters bevatten.",
	"user" => "Uw e-mail adress word niet herkend..",
	"token" => "Deze wachtwoord reset token is niet valide.",
	"sent" => "Er is een email gestuurd met een link om uw wachtwoord te resetten.",
	"reset" => "Uw wachtwoord is gereset.",

];
