<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Categories extends Eloquent{
    
    protected $table = 'categories';
    
    public function applications(){
        
        return $this->belongsToMany('App\Apps');
        
    }

    public function scopeZoek($query, $zoekopdracht)
    {
        return $query->where('name', 'LIKE', '%' . $zoekopdracht . '%');
    }

    public function scopeCategorie($query, $categorie){
        return $query->find($categorie)->applications;
    }

    public function scopeLeeftijd($query, $leeftijd)
    {
        return $query->where('minimal', '<=', $leeftijd)
            ->where('maximal', '>=', $leeftijd);
    }
}
