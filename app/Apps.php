<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Apps extends Eloquent
{
    protected $fillable = ['name',
                            'description',
                            'pic',
                            'android_link',
                            'ios_link',
                            'android_price',
                            'ios_price',
                            'minimal',
                            'maximal'];

    public function images()
    {
        return $this->hasMany('App\Images');
    }
    public function categories()
    {
        return $this->belongsToMany('App\Categories');
    }

    public function reviews()
    {
        return $this->hasMany('App\Reviews');
    }
    
    

    public function recalculateRating()
    {
        $reviews = $this->reviews()->notSpam()->approved();
        $avgRating = $reviews->avg('rating');
        $this->rating_cache = round($avgRating,1);
        $this->rating_count = $reviews->count();
        $this->save();
    }
    public function scopeGratis($query)
    {
        return $query->where(function ($q) {
            $q
                ->where('android_price', '=', 'Gratis')
                ->orWhere('ios_price', '=', 'Gratis');
        });
    }

    public function scopeBetaald($query)
    {
        return $query->where(function ($q) {
            $q
                ->where('android_price', '!=', 'Gratis')
                ->orWhere('ios_price', '!=', 'Gratis');
        });
    }

    public function scopeZoek($query, $zoekopdracht)
    {
        return $query->where('name', 'LIKE', '%' . $zoekopdracht . '%');
    }

    public function scopeLeeftijd($query, $leeftijd)
    {
        return $query->where('minimal', '<=', $leeftijd)
            ->where('maximal', '>=', $leeftijd);
    }
}
