<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Reviews extends Eloquent {
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function applications()
    {
        return $this->belongsTo('App\Apps');
    }

    public function scopeApproved($query)
    {
        return $query->where('approved', true);
    }

    public function scopeSpam($query)
    {
        return $query->where('spam', true);
    }

    public function scopeNotSpam($query)
    {
        return $query->where('spam', false);
    }

    // this function takes in product ID, comment and the rating and attaches the review to the product by its ID, then the average rating for the product is recalculated
    public function storeReviewForApp($appID, $comment, $rating)
    {
        $app = Apps::find($appID);

        // this will be added when we add user's login functionality
        $this->user_id = \Auth::user()->id;

        $this->comment = $comment;
        $this->rating = $rating;
        $app->reviews()->save($this);

        // recalculate ratings for the specified product
        $app->recalculateRating();
    }

    // Validation rules for the ratings
    public function getCreateRules()
    {
        return array(
            'comment'=>'required|min:10',
            'rating'=>'required|integer|between:1,5'
        );
    }

    // Attribute presenters
    public function getTimeagoAttribute()
    {
        $date = \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
        return $date;
    }
}