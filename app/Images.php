<?php
/**
 * Created by PhpStorm.
 * User: Neville
 * Date: 16-04-15
 * Time: 12:21
 */

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
class Images extends Eloquent
{
    public function applications()
    {
        return $this->belongsTo('App\Apps');
    }
}