<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
 // Welke URL naar welke classe en functie door verwijst)
get('home/index', 'PagesController@index');
get('search/form', 'PagesController@zoeken');
get('detail/{id}', 'PagesController@detail');
get('home/login', 'PagesController@detail');
get('unset', 'PagesController@unsetage');

post('search/query', 'PagesController@search');
post('search/age', 'PagesController@age');
get('popup1', 'PagesController@popup1');
post('popup', 'PagesController@popup');
get('search/category/{id}', 'PagesController@category');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);


    resource('admin', 'AdminController');

Route::get('home', 'PagesController@index');
Route::post('detail/{id}', array('before'=>'csrf', function($id)
{
    $input = array(
        'comment' => Input::get('comment'),
        'rating'  => Input::get('rating')
    );
    // instantiate Rating model
    $review = new App\Reviews;
    // Validate that the user's input corresponds to the rules specified in the review model
    $validator = Validator::make( $input, $review->getCreateRules());
    // If input passes validation - store the review in DB, otherwise return to product page with error message
    if ($validator->passes()) {
        $review->storeReviewForApp($id, $input['comment'], $input['rating']);
        return Redirect::to('detail/'.$id.'#reviews-anchor')->with('review_posted',true);
    }

    return Redirect::to('detail/'.$id.'#reviews-anchor')->withErrors($validator)->withInput();
}));