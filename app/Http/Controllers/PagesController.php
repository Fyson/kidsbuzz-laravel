<?php namespace App\Http\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;

use App\Apps;
use App\Categories;

// Class en zijn extends worden hieronder gedefinierd)
class PagesController extends Controller
{

    // Functie voor de homepagina
    public function index(Categories $category)
    {
        if (\Session::has('leeftijd')){
            $leeftijd = \Session::get('leeftijd');

            if ($leeftijd == 'alle') {
                $categorie = $category->get();
            }

            else {
                $categorie = $category->with(['applications' => function ($query) {
                    $leeftijd = \Session::get('leeftijd');

                    $query->where('minimal', '<=', $leeftijd)
                        ->where('maximal', '>=', $leeftijd);
                }])->get();
            }
        }

        else {
            $categorie = $category->get();
        }

        return view('/pages/homepage', compact('categorie'));

    }

    // Functie voor de appdetailpagina
    public function detail(Apps $app, $id)
    {
        //Details van de geselecteerde app uit de database halen
        $app = $app->whereId($id)->first();
        $imgcount = Apps::find($id)->images->count();

        $reviews = $app->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(3);
        $allReviews = $app->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100);

        return view('/pages/detailpagina', compact('app', 'reviews', 'allReviews', 'imgcount'));

    }

    // Doorwijzing naar zoekpagina
    public function zoeken()
    {
        //Categorieën uit de database halen en weergeven in het formulier
        $categorie = Categories::lists('name', 'id');
        return view('/pages/zoekpagina', compact('categorie'));

    }

    /**
     * @param Apps $app
     * @param Request $request
     * @return \Illuminate\View\View
     */
    // Zoekfunctie
    public function search(Apps $app, Categories $cat)
    {

        //Zoekopdracht ophalen uit het formulier
        $zoekopdracht = \Request::input('zoekopdracht');

        //Opgegeven prijs ophalen uit het formulier
        $prijs = \Request::input('prijs');

        //Query voorbereiden op basis van opgegeven prijs
        $query = $app->query();

        //Als er geen prijs is opgegeven haal alle apps uit de database
        if (is_null($prijs)) {
            $query = $app->zoek($zoekopdracht);

            //Als alle apps worden geselecteerd haal alle apps uit de database
        } elseif ($prijs === "alle") {
            $query = $app->zoek($zoekopdracht);

            //Als gratis apps worden geselecteerd haal alle gratis apps uit de database
        } elseif ($prijs === "gratis") {
            $query = $app->gratis()->zoek($zoekopdracht);

            //Als betaalde apps worden geselecteerd haal alle betaalde apps uit de database
        } elseif ($prijs === "betaald") {
            $query = $app->betaald()->zoek($zoekopdracht);
        }

        //Voer de gekozen query uit
        $zoekresultaten = $query->get();


        //Het aantal rijen uit de database ophalen
        $rows = $query->count();

        //Haal alle categorieën uit de database
        $categorie = $cat->get();

        return view('/pages/zoekresultaat', compact('zoekresultaten', 'rows'));
    }

    public function age(Apps $app)
    {

        //Leeftijd ophalen uit het formulier
        $leeftijd = \Input::get('leeftijd');

        /*Haal alle apps uit de database waar de minimale leeftijd lager is
        En de maximale leeftijd hoger is dan de opgegeven leeftijd*/
        $zoekresultaten = $app->leeftijd($leeftijd)->get();

        //Haal het aantal rijen op uit de database
        $rows = $zoekresultaten->count();

        return view('/pages/zoekresultaat', compact('zoekresultaten', 'rows'));
    }

    public function popup ()
    {
        $leeftijd = \Input::get('leeftijd');

        \Session::put('leeftijd', $leeftijd);

        return \Redirect::to('home/index');

    }

    public function popup1 ()
    {
        $leeftijd = "alle";

        \Session::put('leeftijd', $leeftijd);

        return \Redirect::to('home/index');

    }

    public function unsetage ()
    {
        \Session::forget('leeftijd');

        return \Redirect::to('home/index');
    }

//Zoekfunctie
    public function category(Categories $category, $id)
    {


        //Alle apps van de gekozen categorie ophalen uit de database.
        $zoekresultaten = Categories::find($id)->applications;

        //Haal het aantal rijen op uit de database.
        $rows = $zoekresultaten->count();

        return view('/pages/zoekresultaat', compact('zoekresultaten', 'rows'));
    }

    public function review(Apps $app, $id)
    {
        $input = array(
            'comment' => Input::get('comment'),
            'rating'  => Input::get('rating')
        );
        // instantiate Rating model
        $review = new Review;

        // Validate that the user's input corresponds to the rules specified in the review model
        $validator = Validator::make( $input, $review->getCreateRules());

        // If input passes validation - store the review in DB, otherwise return to product page with error message
        if ($validator->passes()) {
            $review->storeReviewForApp($id, $input['comment'], $input['rating']);
            return Redirect::to('detail/'.$id.'#reviews-anchor')->with('review_posted',true);
        }

        return Redirect::to('detail/'.$id.'#reviews-anchor')->withErrors($validator)->withInput();
    }
}
