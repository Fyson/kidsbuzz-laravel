<?php
/**
 * Created by PhpStorm.
 * User: Neville
 * Date: 23-04-15
 * Time: 17:18
 */

namespace App\Http\Controllers;

use App\Apps;
use App\Categories;
use App\Http\Requests\AppRequest;
use App\Http\Requests;


class AdminController extends Controller
 {

    /**
     *Controleren of de gebruiker admin is
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Formulier om nieuwe applicatie toe te voegen
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categorie = Categories::lists('name', 'id');

        return view('/admin/newapp', compact('categorie'));
    }

    /**
     * Applicatie opslaan in de database
     *
     * @param AppRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(AppRequest $request)
    {
        $file = $request->file('pic');
        if (isset($file)) {
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put('/pictogram' . $file->getFilename() . '.' . $extension, File::get($file));
        }
       $app = Apps::create($request->all());

        $app->categories()->sync($request->input('category'));

        session()->flash('flash_message', 'De nieuwe app is toegevoegd!');

        return redirect('/admin/newapp');
    }

    /**
     * Formulier om applicatie te bewerken
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $app = Apps::findOrFail($id);

        $category = $app->categories->lists('id');

        $categorie = Categories::lists('name', 'id');

        return view ('admin.editapp', compact('app', 'categorie', 'category'));
    }

    public function update($id, AppRequest $request)
    {
        $app = Apps::findOrFail($id);

        $app->update($request->all());

        $app->categories()->sync($request->input('category'));

        session()->flash('flash_message', 'De app is bijgewerkt!');

        return redirect()->back();
    }
}